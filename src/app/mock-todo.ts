import { Todo } from './todo';

export const TODOS: Todo[] = [
  {
    id: 10,
    name: 'vaisselle',
    detail: 'Ça peut attendre trois ans',
    deadline: '2023-06-03',
    status: 0,
  },
  {
    id: 11,
    name: 'courses',
    detail: 'Oublie pas le pain',
    deadline: '2022-01-21',
    status: 0,
  },
  {
    id: 12,
    name: 'tp-angular',
    detail: 'Tu me committes tout ça et tu push',
    deadline: '2022-01-20',
    status: 1,
  },
  {
    id: 13,
    name: 'tp-symfony',
    detail: 'Il marche pas en entier mais on est quand même pas mauvais',
    deadline: '2022-01-20',
    status: 2,
  },
];
