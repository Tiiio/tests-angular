import { Component, OnInit } from '@angular/core';
import { Todo } from '../todo';
import { TODOS } from '../mock-todo';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css'],
})
export class TodoComponent implements OnInit {
  todos = TODOS;

  selectedTodo?: Todo;

  onSelect(todo: Todo): void {
    this.selectedTodo = todo;
  }

  statusToString(todo: Todo): string {
    switch (todo.status) {
      case 0:
        return 'À faire';
      case 1:
        return 'En cours';
      case 2:
        return "C'est fait !";
      default:
        return 'something bad happened in todo.components.ts';
    }
  }

  constructor() {}

  ngOnInit(): void {}
}
