export interface Todo {
  id: number;
  name: string;
  detail: string;
  deadline: string;
  // 0: to do
  // 1: in progress
  // 2: done
  status: number;
}
